const config = new URL("postgres://pmsnhnupwpbacs:c47957cd74ff644b91c43d3d0e6294f22169ee24034170a89505b9d05b3fbd48@ec2-52-73-184-24.compute-1.amazonaws.com:5432/d11a0a87gksmru")
console.log("protocol", config.protocol)
console.log("hostname", config.hostname)
console.log("username", config.username)
console.log("password", config.password)
console.log("path",     config.pathname)




const {
  DB_USER = config.username,
  DB_PASSWORD = config.password,
  DB_NAME = config.pathname.replace("/", ""),
  DB_HOST = config.hostname,
  DB_PORT = "5432",
} = process.env;

module.exports = {
  development: {
    username: DB_USER,
    password: DB_PASSWORD,
    database: DB_NAME,
    host: DB_HOST,
    port: DB_PORT,
    dialect: "postgres"
  },
  test: {
    username: DB_USER,
    password: DB_PASSWORD,
    database: `${DB_NAME}_test`,
    host: DB_HOST,
    port: DB_PORT,
    storage: "node_modules/test.sqlite",
    dialect: "sqlite"
  },
  production: {
    username: DB_USER,
    password: DB_PASSWORD,
    database: DB_NAME,
    host: DB_HOST,
    port: DB_PORT,
    dialect: "postgres"
  }
}

